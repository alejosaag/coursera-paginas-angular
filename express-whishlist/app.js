var express = require('express');
var cors = require('cors');

var app = express();

app.use(express.json());
app.use(cors());
app.listen(3000, () => console.log('Server running on port 3000'));

var ciudades = ["Krypton", "Cybertron", "Olimpo", "Daxam"];
var misCiudades = [];

app.get('/ciudades', (req, res, next) => {
    res.json(ciudades.filter((c) => c.toLowerCase().indexOf(req.query.q.toString().toLowerCase()) > -1));
});

app.get('/my', (req, res, next) => res.json(misCiudades));

app.post('/my', (req, res, next) => {
    console.log(req.body);
    misCiudades.push(req.body.nuevo);
    res.json(misCiudades);
});

app.get('/api/translation', (req, res, next) => res.json([
    { lang: req.query.lang, key: 'greeting', value: `Hola ${req.query.lang}` }
]));
