import { DestinoViaje } from './destino-viaje.model';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';

// Estado
export interface DestinosViajesState {
    destinos: DestinoViaje[];
    loading: boolean;
    favorito: DestinoViaje;
}

export function initializeDestinosViajesState() {
    return {
        destinos: [],
        loading: false,
        favorito: null
    };
}

// Acciones
export enum DestinosViajesActionType {
    NUEVO_DESTINO = '[Destinos Viajes] Nuevo',
    ELEGIDO_FAVORITO = '[Destinos Viajes] Favorito',
    VOTE_UP = '[Destinos Viajes] Vote Up',
    VOTE_DOWN = '[Destinos Viajes] Vote Down',
    INIT_MY_DATA = '[Destinos Viajes] Init My Data'
}

export class NuevoDestinoAction implements Action {
    type = DestinosViajesActionType.NUEVO_DESTINO;
    constructor(public destino: DestinoViaje) {}
}

export class ElegidoFavoritoAction implements Action {
    type = DestinosViajesActionType.ELEGIDO_FAVORITO;
    constructor(public destino: DestinoViaje) {}
}

export class VoteUpAction implements Action {
    type = DestinosViajesActionType.VOTE_UP;
    constructor(public destino: DestinoViaje) {}
}

export class VoteDownAction implements Action {
    type = DestinosViajesActionType.VOTE_DOWN;
    constructor(public destino: DestinoViaje) {}
}

export class InitMyDataAction implements Action {
    type = DestinosViajesActionType.INIT_MY_DATA;
    constructor(public destinos: string[]) {}
}

export type DestinosViajesActions = NuevoDestinoAction | ElegidoFavoritoAction | VoteUpAction | VoteDownAction | InitMyDataAction;

// Reducers
export function reducerDestinosViajes(state: DestinosViajesState, action: DestinosViajesActions): DestinosViajesState {
    switch (action.type) {
        case DestinosViajesActionType.NUEVO_DESTINO: {
            return {
                ...state,
                destinos: [...state.destinos, (action as NuevoDestinoAction).destino]
            };
        }
        case DestinosViajesActionType.ELEGIDO_FAVORITO: {
            state.destinos.forEach(x => x.setSelected(false));
            const fav: DestinoViaje = (action as ElegidoFavoritoAction).destino;
            fav.setSelected(true);

            return {
                ...state,
                favorito: fav
            };
        }
        case DestinosViajesActionType.VOTE_UP: {
            const destino: DestinoViaje = (action as VoteUpAction).destino;
            destino.voteUp();

            return { ...state };
        }
        case DestinosViajesActionType.VOTE_DOWN: {
            const destino: DestinoViaje = (action as VoteDownAction).destino;
            destino.voteDown();

            return { ...state };
        }
        case DestinosViajesActionType.INIT_MY_DATA: {
            const destinos: string[] = (action as InitMyDataAction).destinos;
            return {
                ...state,
                destinos: destinos.map((destino) => new DestinoViaje(destino, ''))
            };
        }
    }

    return state;
}

// Effects
@Injectable()
export class DestinosViajesEffects {
    @Effect()
    nuevoAgregado$: Observable<Action> = this.actions$.pipe(
        ofType(DestinosViajesActionType.NUEVO_DESTINO),
        map((action: NuevoDestinoAction) => new ElegidoFavoritoAction(action.destino))
    );

    constructor(private actions$: Actions) {}
}
