import { DestinosViajesState, initializeDestinosViajesState, InitMyDataAction, NuevoDestinoAction, reducerDestinosViajes } from './destino-viaje-state.model';
import { DestinoViaje } from './destino-viaje.model';

describe('reducerDestinosViajes', () => {
    it('should reduce init data', () => {
        // setup
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: InitMyDataAction = new InitMyDataAction(['destino 1', 'destino 2']);
        // action
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        // assertions
        expect(newState.destinos.length).toEqual(2);
        expect(newState.destinos[0].nombre).toEqual('destino 1');
    });

    it('should reduce new item added', () => {
        const prevState: DestinosViajesState = initializeDestinosViajesState();
        const action: NuevoDestinoAction = new NuevoDestinoAction(new DestinoViaje('cali', 'url'));
        const newState: DestinosViajesState = reducerDestinosViajes(prevState, action);
        expect(newState.destinos.length).toEqual(1);
        expect(newState.destinos[0].nombre).toEqual('cali');
    });
});
