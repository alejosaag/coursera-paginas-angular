import { v4 as uuid } from 'uuid';

export class DestinoViaje {
    selected: boolean;
    servicios: string[];
    id = uuid();

    constructor(public nombre: string, public image: string, public votes: number = 0) {
        this.servicios = ['desayuno', 'cena'];
    }

    isSelected(): boolean {
        return this.selected;
    }

    setSelected(selected: boolean): void {
        this.selected = selected;
    }

    voteUp(): void {
        this.votes++;
    }

    voteDown(): void {
        this.votes--;
    }
}
