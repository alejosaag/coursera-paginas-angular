import { forwardRef, Inject, Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppConfig, AppState, APP_CONFIG } from '../app.module';
import { HttpClient, HttpClientModule, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { ElegidoFavoritoAction, NuevoDestinoAction } from '../models/destino-viaje-state.model';
import { DestinoViaje } from './../models/destino-viaje.model';
import { db } from '../inits/DataBase';

@Injectable()
export class DestinosApi {
    destinos: DestinoViaje[] = [];

    constructor(private store: Store<AppState>, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig, private http: HttpClient) {
        this.store
            .select(state => state.destinos)
            .subscribe(data => {
                this.destinos = data.destinos;
            });
    }

    add(destino: DestinoViaje) {
        const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN': 'token-seguridad'});
        const req = new HttpRequest('POST', `${this.config.apiEndpoint}/my`, { nuevo: destino.nombre }, { headers });
        this.http.request(req).subscribe((data: HttpResponse<{}>) => {
            if (data.status === 200) {
                this.store.dispatch(new NuevoDestinoAction(destino));
                // Almacenar en base de datos local
                const myDb = db;
                myDb.table('destinos').put({...destino});
                console.log('destinos almacenados:');
                myDb.destinos.toArray().then(destinosDb => console.log(destinosDb));
            }
        });
    }

    elegir(destino: DestinoViaje) {
        this.store.dispatch(new ElegidoFavoritoAction(destino));
    }

    getById(id: string): DestinoViaje {
        return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
    }
}
