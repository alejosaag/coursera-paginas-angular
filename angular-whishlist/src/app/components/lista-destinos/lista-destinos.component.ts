import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from './../../../app/app.module';

import { DestinoViaje } from './../../models/destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destino-viaje-state.model';
import { DestinosApi } from './../../services/destinos-api-client';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApi]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];
  all;

  constructor(public destinosApiClient: DestinosApi, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.store.select(state => state.destinos.favorito)
      .subscribe(data => {
        if (data !== null) {
          this.updates.push(`Se ha elegido a ${data.nombre}`);
        }
      });

    store.select(state => state.destinos.destinos).subscribe(items => this.all = items);
  }

  ngOnInit(): void {
  }

  agregado(destino: DestinoViaje): void {
    this.destinosApiClient.add(destino);
    this.onItemAdded.emit(destino);
  }

  elegido(destino: DestinoViaje): void {
    this.destinosApiClient.elegir(destino);
  }
}
