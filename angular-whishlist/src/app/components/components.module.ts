import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DestinoViajeComponent } from './destino-viaje/destino-viaje.component';
import { ListaDestinosComponent } from './lista-destinos/lista-destinos.component';
import { DestinoDetalleComponent } from './destino-detalle/destino-detalle.component';
import { FormDestinoViajeComponent } from './form-destino-viaje/form-destino-viaje.component';
import { LoginComponent } from './login/login.component';
import { ProtectedComponent } from './protected/protected.component';
import { VuelosComponent } from './vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponent } from './vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponent } from './vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponent } from './vuelos/vuelos-detalle-component/vuelos-detalle-component.component';



@NgModule({
  declarations: [DestinoViajeComponent, ListaDestinosComponent, DestinoDetalleComponent, FormDestinoViajeComponent, LoginComponent, ProtectedComponent, VuelosComponent, VuelosMainComponent, VuelosMasInfoComponent, VuelosDetalleComponent],
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    DestinoViajeComponent,
    ListaDestinosComponent,
    DestinoDetalleComponent,
    FormDestinoViajeComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponent,
    VuelosMainComponent,
    VuelosMasInfoComponent,
    VuelosDetalleComponent
  ]
})
export class ComponentsModule { }
