import { Injectable } from "@angular/core";
import { Dexie } from 'dexie';
import { DestinoViaje } from '../models/destino-viaje.model';
import { Translation } from '../models/translation-model';

@Injectable({
    providedIn: 'root'
})
export class DataBase extends Dexie {
    destinos: Dexie.Table<DestinoViaje, number>;
    translations: Dexie.Table<Translation, number>;

    constructor() {
        super('DestinosDB');

        this.version(1).stores({
            destinos: '++idd, nombre, image',
        });
        this.version(2).stores({
            destinos: '++idd, nombre, image',
            translations: '++id, lang, key, value',
        });

        this.destinos = this.table('destinos');
        this.translations = this.table('translations');
    }
}

export const db = new DataBase();
