/// <reference types="cypress" />

describe('ventana principal', () => {
    beforeEach(() => {
      cy.visit('http://localhost:4200')
    });

    it('tiene encabezado y en español por defecto', () => {
        cy.visit('http://localhost:4200');
        cy.contains('angular-whitelist');
        cy.get('h1 b').should('contain', 'es');
    });

    it('digita nombre ciudad', () => {
        cy.get('input#nombre').type('Cali').should('have.value', 'Cali')
    });
});
